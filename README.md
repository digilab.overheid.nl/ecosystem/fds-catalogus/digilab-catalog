# digilab-catalog

This repo contains a [TTL](https://www.w3.org/TR/turtle/) file with a [DCAT](https://www.w3.org/TR/vocab-dcat-2/) catalog of data sources that are developed by the [Digilab](https://digilab.overheid.nl/).
